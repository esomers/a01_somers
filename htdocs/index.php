<html>
<head>
	<title>Assignment 01 – Somers, Eli</title>
	<meta charset="utf-8">
</head>
<body>
	<h1><center>Assignment 01 – Somers, Eli</center></h1>
	<div>
		<h2 id="Summary">Summary</h2>
		My name is Eli and I'm a programmer.</br>
		I like to play board and video games as well as do crafts in my free time!
	</div>
	<div>
		<h2 id="Personal">Personal Information</h2>
	
		<b>Name</b>: Eli Somers</br>

		<b>Address</b>: 123 Fourth St. </br>
		Fifth City, OH 67890</br>
		
		<b>Phone Number</b>: 123-456-7890</br>
		
		<b>Email</b>: esomers@bgsu.edu
		
	</div>
	<div>
		<h2 id="Academic">Academic Information</h2>
		<ul>
			<li>Twinsburg High School</li>
			<li>Cuyahoga Valley Career Center (Computer Aided Design)</li>
			<li>The Ohio State University (Engineering)</li>
			<li>Bowling Green State University (Computer Science)</li>
		</ul>
	</div>
	<div>
		<h2 id="Employment">Employment Information</h2>
		<ul>
			<li>Line Worker at Panera Bread</li>
			<li>Supplimental Instruction Leader for CS2020</li>
			<li>Information Technology Intern at Intellicorp Records</li>
		</ul>
	</div>
</body>
</html>